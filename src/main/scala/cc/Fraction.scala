package cc

/**
  * Created by mark on 16/04/2017.
  */
case class Fraction(n:Int, d:Int) {
  require(d!=0,"想你的夜，多希望你能在我身邊")
  //計算最大公因數(greatest common divisor)
  def calGCD(a:Int, b:Int):Int ={
    if(b==0 ) a else calGCD (b,a%b)
  }

  def reduce()={
    val gcd=calGCD(n,d)
    Fraction(n/gcd,d/gcd)
  }
  def plus(that:Fraction): Fraction ={
    Fraction(this.n*that.d+this.d*that.n,this.d*that.d).reduce()
  }
  override def toString: String = n+"/"+d

  def equal (that:Fraction):Boolean={
    this.n*that.d==this.d*that.n
  }

}

object Fraction{
  def apply(n: Int): Fraction = new Fraction(n, 1)
}


